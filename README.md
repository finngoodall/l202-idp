# L202 IDP

Repository for L202 IDP


## Change Log:

**Updating Robot Model:**
- Updated model to reflect dimensions of CAD model

- Caused issues with robot robot entering too far into the zone upon returning with cube

- Fixed by altering "move_to_position" function such that the distance to the position required before stopping is now 0.25 instead of 0.05, seems to have worked

- Observation: Upon grabbing a cube, if robot does not succesfully grab block, will indefinitley move forward as never grabs cube. Will need detection system such as if block no longer in line of sight  then stop moving. Issue may be able to be avoided entirely if robot gets inline with cube centre instead of edge of cube.

- Note: Modifications to dimensions have made robot signigficantly slower, and dimesions have over doubled

**Remember robot position**
- Added and modified functions to allow the robot to remember its orientation before spotting a cube

- Allows robot to return to that direction after resetting to prevent the same area being swept out multiple times during a search

**Remeber cube location**
- When finding an incorrect cube, robots now store the cube's location before resetting.

- When detecting a cube, robots now check to see if the cube is an incorrect colour cube that has already been detected. If it is, they will ignore it.

**Avoid collection zones**
- Robots now avoid moving through collection zones when moving to check a cube or resetting their position

## Problems and Soltuions record

**Issue A: Robot not remembering the location of incorrect coloured cubes leads to checking the cubes multiple times**

When a cube's colour is checked, if it is an incorrect colour, find approx. location of cube centre.
Cube center is at get_object_position * mean_dist_to_center * direction of robot, where mean distance to center is 0.5 * (0.05/sqrt2 + 0.05/2), i.e. mean of distance to center if looking at at cube corner and distance to center if looking perpendicular to a cube edge.
This cube position is stored by the robot.

When searching for blocks, if a point on a spotted cube is within a circle of radius 0.06 m around a center of a  bad block, then report the detected position as being on a bad block, so ignore this detected block.

*This may leave an error for 2 blocks very close to each other so that detecting a point on one block is within the detection radius of the other, but the robot should eventually see the rest of the unknown block and recognise it as a separate block from the bad block.*
*Very clunky solution. Some functions in there to find the cube center 100% accurately using method on ![this diagram](https://imgur.com/EiKi9dT) but getting this working is difficult.*

**Issue B: Robot sweeping out multiple areas**

After resetting, the robot would point in an arbritrary direction from it's reset algorithm. This would lead to the robot sweeping out the same area multiple times when searching for the next cube.

Recording the direction of the robot when moving to check a cube allows the robot to return to this position once reset,meaning the same area is not swept out multiple times.

**Issue C: Robot moving through collection zone**

When moving, the robot normally travels in straight lines. This can result in the robot moving through a collection zone and knocking out all the cubes in the zone.

Implementing a form of the BUG algorithm solves this issue. The adaptation of the BUG algorithm has less accurate edge tracing of the 'obstalce' (collection zone), moving in a circle around the centre of the zone instead for ease of implementation. The circle's radius is chosen to avoid the centre of the zone, where cubes will be dropped off. 