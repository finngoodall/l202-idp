"""
idp_controller
controller to test robots avoiding colelction zone
"""

# import modules
from controller import Robot, Motor, DistanceSensor, LED, GPS, Emitter
import idp_controller_navigationmapping as navmap
import idp_controller_cubecontrol as cubecontrol
import numpy as np
import sensor_functions as sf
import position_functions as pf

# get the time step of the current world.
TIME_STEP = 8
colour = 'red'

# get the Robot instance.
robot = Robot()

# initialise devices
ds = []
dsNames = ['ds1']
for i in range(len(dsNames)):
    ds.append(robot.getDevice(dsNames[i]))
    ds[i].enable(TIME_STEP)

ls = []
lsNames = ['ls1']
for i in range(len(lsNames)):
	ls.append(robot.getDevice(lsNames[i]))
	ls[i].enable(TIME_STEP)

leds = []
ledsNames = ['led1']
for i in range(len(ledsNames)):
	leds.append(robot.getDevice(ledsNames[i]))
	leds[i].set(0)

gpss = []
gpssNames = ['gps1']
for i in range(len(gpssNames)):
	gpss.append(robot.getDevice(gpssNames[i]))
	gpss[i].enable(TIME_STEP)

compasses = []
compassesNames = ['compass1']
for i in range(len(compassesNames)):
	compasses.append(robot.getDevice(compassesNames[i]))
	compasses[i].enable(TIME_STEP)

emitters = []
emittersNames = ['emitter1']
for i in range(len(emittersNames)):
        emitters.append(robot.getDevice(emittersNames[i]))

receivers = []
receiversNames = ['receiver1']
for i in range(len(receiversNames)):
        receivers.append(robot.getDevice(receiversNames[i]))
        receivers[i].enable(TIME_STEP)

# initialise motors
wheels = []
wheelsNames = ['wheel_right', 'wheel_left']
for i in range(len(wheelsNames)):
    wheels.append(robot.getDevice(wheelsNames[i]))
    wheels[i].setPosition(float('inf'))
    wheels[i].setVelocity(0.0)

grabbers = []
grabberNames = ['grabber_right', 'grabber_left']
for i in range(len(grabberNames)):
	grabbers.append(robot.getDevice(grabberNames[i]))
	grabbers[i].setPosition(-0.3)
	grabbers[i].setVelocity(float('inf'))


# main loop:

# initialise flags
cubeGrabbed = False
cubeSpotted = False
cubeCentred = False
needsReset = False
closeToCube = True
atResetPos = False
deadZone = False
needGrab = 2

# initialise variables
cubeCentredCounter = 0
blockCounter = 0
initialPauseCounter = 20
badCubes = []
#prev_c = np.array([0, 0])
robotDirection = np.array([0, 0])
otherPosition = np.array([0, 0])
otherDirection = np.array([0, 0])

# wait 1 timestep to initialise more start variables
robot.step(TIME_STEP)
startPosition = np.array([gpss[0].getValues()[0], gpss[0].getValues()[2]])
zonePosition = pf.get_zone_position(colour)


while initialPauseCounter > 0:
        robot.step(TIME_STEP)
        initialPauseCounter -= 1

print("RED: START!")

# main loop
while robot.step(TIME_STEP) != -1:

	# get sensor readings
	pulsewidth = ds[0].getValue()
	dist = sf.ultrasonicDistance(pulsewidth)
	p = sf.get_object_position(dist, gpss[0], compasses[0])
	light = ls[0].getValue()
	sf.send_coordinate_data(gpss[0], compasses[0], emitters[0])
	otherPosition, otherDirection = sf.recieve_coordinate_data(otherPosition, otherDirection, receivers[0])
	nearZoneFlag, nearZoneColour = pf.nearCollectionZone(gpss[0], compasses[0])

	if nearZoneFlag and np.linalg.norm(pf.get_robot_position(gpss[0]) - pf.get_zone_position(nearZoneColour)) < 0.3:
		# align perpendicular to zone centre
		if pf.get_relative_orientation(gpss[0], compasses[0], pf.get_zone_position(nearZoneColour)) > 0.05:
			r_wheel_speed, l_wheel_speed = 1.57, -1.57
		elif pf.get_relative_orientation(gpss[0], compasses[0], pf.get_zone_position(nearZoneColour)) < -0.05:
			r_wheel_speed, l_wheel_speed = -1.57, 1.57
		else:			# robot allowed to move forward
			r_wheel_speed, l_wheel_speed = 3.14, 3.14

		wheels[0].setVelocity(r_wheel_speed)
		wheels[1].setVelocity(l_wheel_speed)		

	else:
		atTarget = navmap.move_to_position(wheels[0], wheels[1], gpss[0], compasses[0], np.array([0, -1]))
		if atTarget:
			break



# end of program
for w in wheels:
	w.setVelocity(0)
print('{} robot has finished!'.format(colour))