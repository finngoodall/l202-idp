"""
Contains functions needed for the mapping and navigation of the robot
"""

import numpy as np
import position_functions as pf
import sensor_functions as sf


# rotates robot to search for cube
def searchForCube(right_wheel, left_wheel, gps, compass, dist, bad_blocks, otherPosition):			# need to implement check for infinite rotations incase of hidden cubes
	right_wheel.setVelocity(1.57)		# rotate robot to search
	left_wheel.setVelocity(-1.57)
			
	return sf.determine_if_block(dist, bad_blocks, gps, compass, otherPosition)		# if looking at a block


# centres the robot so it faces approximately at a cube centre
def centreOnCube(right_wheel, left_wheel, dist, counter, timeStep):
        #Set initial values for counter
        if counter == 0:
                t = 200.0 /  dist                    #Value 200.0 is rougly angular velocity of robot in rads/s multiplied by 1000 to get t in ms
                counter = int(t / timeStep)
                right_wheel.setVelocity(0.2)		
                left_wheel.setVelocity(-0.2)
                return counter, False
        # Whilst counter is greater than one, robot continues to turn and decrease counter by one each timestep
        elif counter > 1:
                counter = counter - 1
                right_wheel.setVelocity(0.2)		
                left_wheel.setVelocity(-0.2)
                return counter, False
        # Counter = 1  ==> returning true
        else:
                return counter, True
        # Function returns both counter value and bool as must update counter outside of function



# checks colour of cube
def checkCube(right_wheel, left_wheel, dist, light, colour):
	if dist > 0.015:
		right_wheel.setVelocity(3.14)		# move forward to cube
		left_wheel.setVelocity(3.14)
		return 2			# 'other' situation (e.g. travelling to block) returns 2 
	else:		# at cube
		right_wheel.setVelocity(0)
		left_wheel.setVelocity(0)
		if sf.determine_block_colour(light, colour):	# correct colour returns 1
			return 1
		else:		# incorrect colour returns 0
			return 0



# moves robot to a target position
def move_to_position(right_wheel, left_wheel, gps, compass, target_position, target_direction=np.array([0,0])):
	relative_orientation = pf.get_relative_orientation(gps, compass, target_position)
	dist_to_target = pf.distance_to(gps, compass, target_position)

	if relative_orientation > -0.99:		# not pointing at target
	    right_wheel.setVelocity(1.57)
	    left_wheel.setVelocity(-1.57)
	    return False
	elif dist_to_target > 0.15:
		if relative_orientation > 0:			# pointing towards target 
		    right_wheel.setVelocity(-3.14)
		    left_wheel.setVelocity(-3.14)		# these reverse/forward wheel velocities look like they're the wrong way round
		    return False
		else:									# pointing away from target
		    right_wheel.setVelocity(3.14)
		    left_wheel.setVelocity(3.14)
		    return False
	else:			# at target	
		return True								


# rotates robot to point in the given direction
def point_towards(right_wheel, left_wheel, compass, target_direction):
	if np.allclose(pf.get_robot_direction(compass), target_direction, rtol = 0.1, atol = 0.1):				# in correct orientation
	    right_wheel.setVelocity(0)
	    left_wheel.setVelocity(0)
	    return True
	else:								# not in correct orientation
	    right_wheel.setVelocity(1.57)
	    left_wheel.setVelocity(-1.57)
	    return False				


# function that records an accurate block centre position, rather than an estimate
# not currently used as it is harder to implement and not worth the time right now
def record_bad_block(right_wheel, left_wheel, gps, compass, dist, bad_blocks, prev_c):
	if dist < 0.1:			# reverse if close to cube
		right_wheel.setVelocity(-1.0)
		left_wheel.setVelocity(-1.0)
		return False, prev_c
	else:					# rotate when far enough from cube
		if determine_if_block(dist, bad_blocks, gps, compass):		# looking at cube
			right_wheel.setVelocity(-0.1)
			left_wheel.setVelocity(0.1)
			p2 = sf.get_object_position(dist, gps, compass)       # edge
			robot.step(timestep)
			p1 = sf.get_object_position(dist, gps, compass)       # corner

			c = sf.find_block_center_better(p1, p2)			# cube centre location from current readings
            
			return False, c
		else:			# no longer looking at cube (i.e. passed cube corner)
			return True, prev_c
			
			
def entered_dead_zone(gps, otherPosition):
    position = pf.get_robot_position(gps)
    radius = np.linalg.norm(position - otherPosition)
    if (radius < 0.5):
            return True
    else:
            return False
                
                
def avoid_other_robot(right_wheel, left_wheel, gps, compass, otherPosition):
    position = pf.get_robot_position(gps)
    radius = np.linalg.norm(position - otherPosition)
    right_wheel.setVelocity(0.0)
    left_wheel.setVelocity(0.0)
    
    if (radius < 0.8):
            return True
    else:
            return False


# avoids the collection zone
def avoidCollectionZone(right_wheel, left_wheel, gps, compass):
	# align perpendicular to zone centre
	if pf.get_relative_orientation(gps, compass, target_position) > 0.05:
		r_wheel_speed, l_wheel_speed = 1.57, -1.57
	else:			# robot allowed to move forward
		r_wheel_speed, l_wheel_speed = 3.14, 3.14

	right_wheel.setVelocity(r_wheel_speed)
	left_wheel.setVelocity(l_wheel_speed)


# moves the robot in a circle around collection zones for avoidance
def avoidCollectionZone(right_wheel, left_wheel):
	# align perpendicular to zone centre
	if pf.get_relative_orientation(gpss[0], compasses[0], pf.get_zone_position(nearZoneColour)) > 0.05:
		r_wheel_speed, l_wheel_speed = 1.57, -1.57
	elif pf.get_relative_orientation(gpss[0], compasses[0], pf.get_zone_position(nearZoneColour)) < -0.05:
		r_wheel_speed, l_wheel_speed = -1.57, 1.57
	else:			# robot allowed to move forward when properly aligned
		r_wheel_speed, l_wheel_speed = 3.14, 3.14

	right_wheel.setVelocity(r_wheel_speed)
	left_wheel.setVelocity(l_wheel_speed)	