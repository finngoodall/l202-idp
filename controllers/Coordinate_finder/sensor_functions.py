"""
Functions to handle inputs from sensors
"""

import math
import position_functions as pf





# converts the pulse width from the ultrasonic sensor to the distance in m
# takes inputs: pulsewidth (float)
# returns distance (float)
def ultrasonicDistance(pulsewidth):
	distance = pulsewidth / 6250		# from HC-SR504 datasheet

	return distance
	
	



# Obtains coordinate position of the object the distance sensor is looking at
def get_object_position(pulsewidth, gps, compass):
    scanned_distance = ultrasonicDistance(pulsewidth) + 0.1
    position_from_robot = scanned_distance * pf.get_robot_direction(compass)
    object_position = position_from_robot + pf.get_robot_position(gps)
    return object_position
    
    
    
     

# Determines if the object the sensor is looking at is a block that should be grabbed or not, returns 1 if block or 0 if not   
def determine_if_block(pulsewidth, gps, compass):
    p = get_object_position(pulsewidth, gps, compass)
    c1 = pf.get_zone_position("red")
    c2 = pf.get_zone_position("green")
    
    #Checks if object looking at is wall
    if abs(p[0]) > 1.15 or abs(p[1]) > 1.15:
        return 0 
        
    #Checks if object looking at is block already in zone
    elif p[0] < (c1[0] + 0.2) and p[0] > (c1[0] - 0.2) and p[1] < (c1[1] + 0.2) and p[1] > (c1[1] - 0.2):
        return 0
        
    elif p[0] < (c2[0] + 0.2) and p[0] > (c2[0] - 0.2) and p[1] < (c2[1] + 0.2) and p[1] > (c2[1] - 0.2):
        return 0
        
    #Check will be implemented here to determine if looking at other robot or not
    
    #If all previous statements untrue, then must be a valid block to grab
    else:
        print("BLOCK!")
        return 1