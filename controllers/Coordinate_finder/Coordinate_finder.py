"""Coordinate_finder controller."""
from controller import Robot

import position_functions as pf
import sensor_functions as sf
import numpy as np

robot = Robot()

timestep = 2048

colour = "green"

#GPS from robot is initialised and collects samples every timestep
gps = robot.getDevice('gps1')
gps.enable(timestep)

#Compass from robot is initialised and collects samples every timestep
compass = robot.getDevice('compass1')
compass.enable(timestep)

ds = []
dsNames = ['ds1']
for i in range(len(dsNames)):
    ds.append(robot.getDevice(dsNames[i]))
    ds[i].enable(timestep)

wheels = []
wheelsNames = ['wheel_left', 'wheel_right']
for i in range(2):
    wheels.append(robot.getDevice(wheelsNames[i]))
    wheels[i].setPosition(float('inf'))
    wheels[i].setVelocity(0.0)

trigger = 1
    
# Main loop:
# - perform simulation steps until Webots is stopping the controller
while robot.step(timestep) != -1:
    """
    relative_orientation = pf.get_relative_orientation_to_zone(gps, compass, colour)
    distance_to_zone = pf.get_distance_to_zone(gps, colour)
    if trigger == 1:
        if relative_orientation > -0.99:
            leftSpeed = 1
            rightSpeed = -1
        elif distance_to_zone > 0.2:
            leftSpeed = 2.0
            rightSpeed = 2.0
        else:
            leftSpeed = 0
            rightSpeed = 0
            trigger = 0
    wheels[0].setVelocity(leftSpeed)
    wheels[1].setVelocity(rightSpeed)
    """
    pulsewidth = ds[0].getValue()
    wheels[0].setVelocity(0.0)
    wheels[1].setVelocity(0.0)
    print(sf.get_object_position(pulsewidth, gps, compass))
 
   
    
    
    # Process sensor data here.

    # Enter here functions to send actuator commands, like:
    #  motor.setPosition(10.0)
    pass

# Enter here exit cleanup code.
