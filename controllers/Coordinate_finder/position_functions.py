import numpy as np



#2D vector of robot position is obtained in x and z directions from GPS
def get_robot_position(gps):
    robot_position = np.array([
        gps.getValues()[0],
        gps.getValues()[2]
        ])
    return robot_position



#2D unit vector is obtained in the x and z direction the front of the robot is currently facing    
def get_robot_direction(compass):
    
    robot_direction = np.array([
            compass.getValues()[2],
            compass.getValues()[0]
            ])
    unit_robot_direction = robot_direction / np.linalg.norm(robot_direction)
    return unit_robot_direction
    
    

def get_zone_position(colour):    
    if colour == "red":
        zone_position = [0, 0.4]
    elif colour == "green":
        zone_position = [0, -0.4]
    return zone_position


        
def get_relative_orientation_to_zone(gps, compass, colour):
    zone_position = get_zone_position(colour)
    robot_position = get_robot_position(gps)
    displacement_to_zone = robot_position - zone_position
    unit_displacement_to_zone = displacement_to_zone / np.linalg.norm(displacement_to_zone)
    relative_orientation = np.dot(get_robot_direction(compass), unit_displacement_to_zone)
    return(relative_orientation)
    
    
def get_distance_to_zone(gps, colour):
    zone_position = get_zone_position(colour)
    robot_position = get_robot_position(gps)
    distance_to_zone = np.linalg.norm(robot_position - zone_position)
    return distance_to_zone
