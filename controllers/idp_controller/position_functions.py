import numpy as np



#2D vector of robot position is obtained in x and z directions from GPS
def get_robot_position(gps):
    robot_position = np.array([
        gps.getValues()[0],
        gps.getValues()[2]
        ])
    return robot_position



#2D unit vector is obtained in the x and z direction the front of the robot is currently facing    
def get_robot_direction(compass):
    
    robot_direction = np.array([
            compass.getValues()[2],
            compass.getValues()[0]
            ])
    unit_robot_direction = robot_direction / np.linalg.norm(robot_direction)
    return unit_robot_direction
    
    
# returns the position of the coloured zone for a robot
def get_zone_position(colour):    
    if colour == "red":
        zone_position = [0, 0.4]
    elif colour == "green":
        zone_position = [0, -0.4]
    return zone_position


# determines if the robot is pointing at a target location
def get_relative_orientation(gps, compass, target_position):
    current_position = get_robot_position(gps)              # get current position and orientation
    current_orientation = get_robot_direction(compass)

    displacement_to_target = current_position - target_position
    unit_displacement_to_target = displacement_to_target / np.linalg.norm(displacement_to_target)       # get unit direction vector to target

    relative_orientation = np.dot(current_orientation, unit_displacement_to_target)     # get relative orientation as dot product

    return relative_orientation


# determines the distance from the robot to a target location
def distance_to(gps, compass, target_position):
    current_position = get_robot_position(gps)              # get current position and orientation
    current_orientation = get_robot_direction(compass)

    displacement_to_target = current_position - target_position

    return np.linalg.norm(displacement_to_target)