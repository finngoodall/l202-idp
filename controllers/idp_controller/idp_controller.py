"""
idp_controller
controller to be used for the robots
"""

# import modules
from controller import Robot, Motor, DistanceSensor, LED, GPS, Emitter
import idp_controller_navigationmapping as navmap
import idp_controller_cubecontrol as cubecontrol
import numpy as np
import sensor_functions as sf
import position_functions as pf

# get the time step of the current world.
TIME_STEP = 8
colour = 'green'

# get the Robot instance.
robot = Robot()

# initialise devices
ds = []
dsNames = ['ds1']
for i in range(len(dsNames)):
    ds.append(robot.getDevice(dsNames[i]))
    ds[i].enable(TIME_STEP)

ls = []
lsNames = ['ls1']
for i in range(len(lsNames)):
	ls.append(robot.getDevice(lsNames[i]))
	ls[i].enable(TIME_STEP)

leds = []
ledsNames = ['led1']
for i in range(len(ledsNames)):
	leds.append(robot.getDevice(ledsNames[i]))
	leds[i].set(0)

gpss = []
gpssNames = ['gps1']
for i in range(len(gpssNames)):
	gpss.append(robot.getDevice(gpssNames[i]))
	gpss[i].enable(TIME_STEP)

compasses = []
compassesNames = ['compass1']
for i in range(len(compassesNames)):
	compasses.append(robot.getDevice(compassesNames[i]))
	compasses[i].enable(TIME_STEP)

emitters = []
emittersNames = ['emitter1']
for i in range(len(emittersNames)):
	emitters.append(robot.getDevice(emittersNames[i]))

# initialise motors
wheels = []
wheelsNames = ['wheel_right', 'wheel_left']
for i in range(len(wheelsNames)):
    wheels.append(robot.getDevice(wheelsNames[i]))
    wheels[i].setPosition(float('inf'))
    wheels[i].setVelocity(0.0)

grabbers = []
grabberNames = ['grabber_right', 'grabber_left']
for i in range(len(grabberNames)):
	grabbers.append(robot.getDevice(grabberNames[i]))
	grabbers[i].setPosition(-0.3)
	grabbers[i].setVelocity(float('inf'))


# main loop:

# initialise flags
cubeGrabbed = False
cubeSpotted = False
cubeCentred = False
needsReset = False
closeToCube = True
atResetPos = False
needGrab = 2

# initialise variables
cubeCentredCounter = 0
blockCounter = 0
badCubes = []
#prev_c = np.array([0, 0])
robotDirection = np.array([0, 0])

# wait 1 timestep to initialise more start variables
robot.step(TIME_STEP)
startPosition = np.array([gpss[0].getValues()[0], gpss[0].getValues()[2]])
zonePosition = pf.get_zone_position(colour)

# main loop
while robot.step(TIME_STEP) != -1:

	# get sensor readings
	pulsewidth = ds[0].getValue()
	dist = sf.ultrasonicDistance(pulsewidth)
	light = ls[0].getValue()

	if not needsReset:			# complete main loop if no reset is needed

		if not cubeSpotted:			# rotate until cube spotted
			cubeSpotted = navmap.searchForCube(wheels[0], wheels[1], gpss[0], compasses[0], dist, badCubes)
		
		if cubeSpotted and not cubeGrabbed:			# cube spotted but not collected by robot
			if not cubeCentred:						# rotate to centre on cube
				cubeCentredCounter, cubeCentred = navmap.centreOnCube(wheels[0], wheels[1], dist, cubeCentredCounter, TIME_STEP)

			else:									# centred on cube so move to cube
				needGrab = navmap.checkCube(wheels[0], wheels[1], dist, light, colour)			# check cube colour
				robotDirection = pf.get_robot_direction(compasses[0])				

				if needGrab == 1:		# cube correct colour
					cubecontrol.close_grabbers(grabbers[0], grabbers[1])
					needGrab = 2
					cubeGrabbed = True
				elif needGrab == 0:		# cube incorrect colour
					badCubes.append(sf.find_block_center(dist, gpss[0], compasses[0]))
					needsReset = True

					"""
					# find center of bad block
					badCubeCheck = navmap.record_bad_block(wheels[0], wheels[1], gpss[0], compasses[0], dist, badCubes, prev_c)
					if not badCubeCheck[0]:
						prev_c = badCubeCheck[1]
					else:
						badCubes.append(prev_c)		# add location of cube center to array of 'bad cubes' to avoid in future
						needsReset = True
					"""
				else:
					pass

		elif cubeGrabbed:		# return to base when cube grabbed
			atBase = navmap.move_to_position(wheels[0], wheels[1], gpss[0], compasses[0], zonePosition)

			if atBase:			# release cube when back at base
				wheels[0].setVelocity(0)
				wheels[1].setVelocity(0)
				cubecontrol.open_grabbers(grabbers[0], grabbers[1])
				
				blockCounter += 1			# iterate block counter, decide if any more blocks need to be collected
				if blockCounter == 4:
					break

				needsReset = True
	
	else:			# need to reset robot for next search
		if dist < 0.2 and closeToCube:		# close to a cube so need to reverse
			wheels[0].setVelocity(-3.14)
			wheels[1].setVelocity(-3.14)
		else:			# moves back to start position
			closeToCube = False
			if not atResetPos:
				atResetPos = navmap.move_to_position(wheels[0], wheels[1], gpss[0], compasses[0], startPosition)
			else:
				needsReset = not(navmap.point_towards(wheels[0], wheels[1], compasses[0], robotDirection))
				if not needsReset:
					print('reset finished')
					cubeGrabbed = False		# resets flags and variables for next run
					cubeSpotted = False
					cubeCentred = False
					closeToCube = True
					atResetPos = False
					needGrab = 2
					robot_direction = 0
					cubeCentredCounter = 0




# end of program
print('{} robot has finished!'.format(colour))