import numpy as np



#2D vector of robot position is obtained in x and z directions from GPS
def get_robot_position(gps):
    robot_position = np.array([
        gps.getValues()[0],
        gps.getValues()[2]
        ])
    return robot_position



#2D unit vector is obtained in the x and z direction the front of the robot is currently facing    
def get_robot_direction(compass):
    
    robot_direction = np.array([
            compass.getValues()[2],
            compass.getValues()[0]
            ])
    unit_robot_direction = robot_direction / np.linalg.norm(robot_direction)
    return unit_robot_direction
    
    
# returns the position of the coloured zone for a robot
def get_zone_position(colour):    
    if colour == "red":
        zone_position = [0, 0.4]
    elif colour == "green":
        zone_position = [0, -0.4]
    return zone_position


# determines if the robot is pointing at a target location
def get_relative_orientation(gps, compass, target_position):
    current_position = get_robot_position(gps)              # get current position and orientation
    current_orientation = get_robot_direction(compass)

    displacement_to_target = target_position - current_position
    unit_displacement_to_target = displacement_to_target / np.linalg.norm(displacement_to_target)       # get unit direction vector to target

    relative_orientation = np.dot(current_orientation, unit_displacement_to_target)     # get relative orientation as dot product

    return relative_orientation


# determines the distance from the robot to a target location
def distance_to(gps, compass, target_position):
    current_position = get_robot_position(gps)              # get current position and orientation
    current_orientation = get_robot_direction(compass)

    displacement_to_target = current_position - target_position

    return np.linalg.norm(displacement_to_target)


# tells if the robot is near a collection zone and pointing into it
# used to stop hit and runs on collected cubes taking away our points :(
def nearCollectionZone(gps, compass):
    rob_pos = get_robot_position(gps)
    rob_dir = get_robot_direction(compass)

    g_pos = get_zone_position('green')
    r_pos = get_zone_position('red')

    # returns true if robot within zone and pointing into zone
    if abs(rob_pos[0] - g_pos[0]) < 0.35 and abs(rob_pos[1] - g_pos[1]) < 0.35:
        return True, 'green'
    elif abs(rob_pos[0] - r_pos[0]) < 0.35 and abs(rob_pos[1] - r_pos[1]) < 0.35:
        return True, 'red'
    else:
        return False, 'none'