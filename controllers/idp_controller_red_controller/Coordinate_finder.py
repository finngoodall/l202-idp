"""Coordinate_finder controller."""
from definitions import robot, timestep, colour
import position_functions as pf
import numpy as np

wheels = []
wheelsNames = ['wheel_left', 'wheel_right']
for i in range(2):
    wheels.append(robot.getDevice(wheelsNames[i]))
    wheels[i].setPosition(float('inf'))
    wheels[i].setVelocity(0.0)

counter = 100
trigger = 1
    
# Main loop:
# - perform simulation steps until Webots is stopping the controller
while robot.step(timestep) != -1:
    relative_orientation = pf.get_relative_orientation_to_zone(colour)
    distance_to_zone = pf.get_distance_to_zone(colour)
    if trigger == 1:
        if relative_orientation < 0.99:
            leftSpeed = 0.5
            rightSpeed = -0.5
        elif counter > 0:
            leftSpeed = 0.0
            rightSpeed = 0.0
            counter = counter - 1
        elif distance_to_zone > 0.2:
            leftSpeed = 2.0
            rightSpeed = 2.0
        else:
            leftSpeed = 0
            rightSpeed = 0
            trigger = 0
    wheels[0].setVelocity(leftSpeed)
    wheels[1].setVelocity(rightSpeed)
    
    # Process sensor data here.

    # Enter here functions to send actuator commands, like:
    #  motor.setPosition(10.0)
    pass

# Enter here exit cleanup code.
