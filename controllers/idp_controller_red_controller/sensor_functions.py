"""
Functions to handle inputs from sensors
"""

import numpy as np
import struct
import position_functions as pf





# converts the pulse width from the ultrasonic sensor to the distance in m
def ultrasonicDistance(pulsewidth):
	distance = pulsewidth / 6250		# from HC-SR504 datasheet

	return distance
	
	



# Obtains coordinate position of the object the distance sensor is looking at
def get_object_position(dist, gps, compass):
    position_from_robot = (dist + 0.1) * pf.get_robot_direction(compass)
    object_position = position_from_robot + pf.get_robot_position(gps)
    return object_position
    
    
    
     

# Determines if the object the sensor is looking at is a block that should be grabbed or not, returns 1 if block or 0 if not   
def determine_if_block(dist, bad_blocks, gps, compass, otherPosition, console = True):
    p = get_object_position(dist, gps, compass)
    c1 = pf.get_zone_position("red")
    c2 = pf.get_zone_position("green")

    # checks if spotted block is a known 'bad block'
    for block in bad_blocks:
        np.linalg.norm(block - p)
        if np.linalg.norm(block - p) < 0.071:      # if point is possibly on a bad block, as described in README for issue A
            return False

    #Checks if object looking at is wall
    if abs(p[0]) > 1.175 or abs(p[1]) > 1.175:
        return False
        
    #Checks if object looking at is block already in zone
    elif p[0] < (c1[0] + 0.2) and p[0] > (c1[0] - 0.2) and p[1] < (c1[1] + 0.2) and p[1] > (c1[1] - 0.2):
        return False
        
    elif p[0] < (c2[0] + 0.2) and p[0] > (c2[0] - 0.2) and p[1] < (c2[1] + 0.2) and p[1] > (c2[1] - 0.2):
        return False

    #Check will be implemented here to determine if looking at other robot or not
    elif np.linalg.norm(p - otherPosition) < 0.3:
        return False
    #If all previous statements untrue, then must be a valid block to grab
    else:
        if console:
            print("RED: BLOCK!")
        return True


# determines if the block is the correct colour for the robot
def determine_block_colour(light, colour):
    if light > 340:
        print('RED: FOUND RED CUBE')
        return True
    else:
        print('RED: FOUND GREEN CUBE')
        return False


# finds the center of a block
def find_block_center(dist, gps, compass):
    p = get_object_position(dist, gps, compass)
    mean_dist_to_c  = 0.5 * 0.05*(np.sqrt(0.5) + 0.5)

    c = p + mean_dist_to_c * pf.get_robot_direction(compass)

    return c


# finds position of centre of a block
# follows method described in README for issue A 
def find_block_center_better(p1, p2):
    rot_mat = np.matrix([[1, 0], [0, 1]])

    delta_p = p2 - p1
    unit_delta_p = delta_p / np.linalg.norm(delta_p)
    unit_delta_c = rot_mat.dot(unit_delta_p)

    c_pos = p1 + 0.05*np.sqrt(0.5)*unit_delta_c
    return c_pos
    
    
def send_coordinate_data(gps, compass, emitter):
        position = pf.get_robot_position(gps)
        direction = pf.get_robot_direction(compass)
        message = struct.pack('ffff', position[0], position[1], direction[0], direction[1])
        emitter.send(message)
        
def recieve_coordinate_data(otherPosition, otherDirection, receiver):
        if receiver.getQueueLength() > 0:
                message = receiver.getData()
                values = struct.unpack('ffff', message)
                otherPosition = np.array([values[0], values[1]])
                otherDirection = np.array([values[2], values[3]])
                receiver.nextPacket()
        return otherPosition, otherDirection