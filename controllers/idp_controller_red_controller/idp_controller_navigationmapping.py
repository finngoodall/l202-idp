"""
Contains functions needed for the mapping and navigation of the robot
"""

import numpy as np
import position_functions as pf
import sensor_functions as sf


# rotates robot to search for cube
def searchForCube(right_wheel, left_wheel, gps, compass, dist, bad_blocks, otherPosition):			# need to implement check for infinite rotations incase of hidden cubes
	right_wheel.setVelocity(1.57)		# rotate robot to search
	left_wheel.setVelocity(-1.57)
			
	return sf.determine_if_block(dist, bad_blocks, gps, compass, otherPosition)		# if looking at a block


# centres the robot so it faces approximately at a cube centre
def centreOnCube(right_wheel, left_wheel, gps, compass, dist, counter, timeStep, bad_blocks, otherPosition, backup):
        emptyPositionArray = np.array([0, 0])
        #Set initial values for counter
        if counter == 0:
                backup = sf.get_object_position(dist, gps, compass)
                t = 150.0 /  dist                    #Value 200.0 is rougly angular velocity of robot in rads/s multiplied by 1000 to get t in ms
                counter = int(t / timeStep)
                right_wheel.setVelocity(0.2)		
                left_wheel.setVelocity(-0.2)
                return counter, False, emptyPositionArray, backup
        # Whilst counter is greater than one, robot continues to turn and decrease counter by one each timestep
        elif counter > 1:
                counter = counter - 1
                right_wheel.setVelocity(0.2)		
                left_wheel.setVelocity(-0.2)
                return counter, False, emptyPositionArray, backup
        # Counter = 1  ==> returning true
        else:
                blockPosition = (sf.get_object_position(dist, gps, compass) + 0.02 * pf.get_robot_direction(compass))
                if sf.determine_if_block(dist, bad_blocks, gps, compass, otherPosition, False):
                        return counter, True, blockPosition, backup
                else:
                        blockPosition = backup
                        return counter, True, blockPosition, backup
        # Function returns both counter value and bool as must update counter outside of function


"""
def centreOnCube(right_wheel, left_wheel, gps, compass, dist, counter, timeStep):
        d = pf.get_robot_direction(compass)
        p = sf.get_object_position(dist, gps, compass)
        cubePosition = p + 0.025 * np.array([-d[1], d[0]])
        return True, cubePosition
        # Function returns both counter value and bool as must update counter outside of function
"""

# checks colour of cube, includes collection zone avoidance
def checkCube(right_wheel, left_wheel, gps, compass, dist, light, colour, cube_position, ignoreZone):
	nearZoneFlag, nearZoneColour = pf.nearCollectionZone(gps, compass)

	if light > 340:
		print("RED: FOUND RED CUBE")
		return 1			# correct colour cube returns 1
	elif not ignoreZone and nearZoneFlag and np.linalg.norm(pf.get_robot_position(gps) - pf.get_zone_position(nearZoneColour)) < 0.3:
		avoidCollectionZone(right_wheel, left_wheel, gps, compass, nearZoneColour)
		return 2			# 'other' situation (i.e. moving to cube) returns 2
	elif dist < 0.02:
		print("RED: FOUND GREEN CUBE")
		return 0			# incorrect colour cube returns 0
	elif np.linalg.norm(pf.get_robot_position(gps) - cube_position) < 0.08:
		return 3
	else:
		atTarget = move_to_position(right_wheel, left_wheel, gps, compass, cube_position)
		return 2

"""
	# avoid collection zone if near to one
	if not ignoreZone and nearZoneFlag and np.linalg.norm(pf.get_robot_position(gps) - pf.get_zone_position(nearZoneColour)) < 0.3:
		avoidCollectionZone(right_wheel, left_wheel, gps, compass, nearZoneColour)
		return 2
	else:				# no zone to worry about
		if dist > 0.015:			# too far away to detect so move towards cube
			atTarget = move_to_position(right_wheel, left_wheel, gps, compass, cube_position)
			return 2				# 'other' situation (e.g. travelling to block) returns 2 
		else:		# at cube
			right_wheel.setVelocity(0)
			left_wheel.setVelocity(0)
			if sf.determine_block_colour(light, colour):	# correct colour returns 1
				return 1
			else:		# incorrect colour returns 0
				return 0
"""

# moves robot to a target position
def move_to_position(right_wheel, left_wheel, gps, compass, target_position, accuracy = 0.995):
	relative_orientation = pf.get_relative_orientation(gps, compass, target_position)
	dist_to_target = pf.distance_to(gps, compass, target_position)
	c = target_position - pf.get_robot_position(gps)
	d = pf.get_robot_direction(compass)
	a = np.array([d[0], 0, d[1]])
	b = np.array([c[0], 0, c[1]])
	cross = np.cross(a, b)

	if relative_orientation < accuracy and dist_to_target > 0.2:        # not pointing at target
		if cross[1] < 0:       			
			right_wheel.setVelocity(-1.57)
			left_wheel.setVelocity(1.57)
		else:
			right_wheel.setVelocity(1.57)
			left_wheel.setVelocity(-1.57)
		return False
	elif dist_to_target > 0.15:
		if relative_orientation < 0:			# pointing towards target 
		    right_wheel.setVelocity(-3.14)
		    left_wheel.setVelocity(-3.14)
		    return False
		else:									# pointing away from target
		    right_wheel.setVelocity(3.14)
		    left_wheel.setVelocity(3.14)
		    return False
	else:			# at target	
		return True							


# rotates robot to point in the given direction
def point_towards(right_wheel, left_wheel, compass, target_direction):
	if np.allclose(pf.get_robot_direction(compass), target_direction, rtol = 0.1, atol = 0.1):				# in correct orientation
	    right_wheel.setVelocity(0)
	    left_wheel.setVelocity(0)
	    return True
	else:								# not in correct orientation
	    right_wheel.setVelocity(1.57)
	    left_wheel.setVelocity(-1.57)
	    return False				


# (redundant) function that records an accurate block centre position, rather than an estimate
# not currently used as it is harder to implement and not worth the time right now
def record_bad_block(right_wheel, left_wheel, gps, compass, dist, bad_blocks, prev_c):
	if dist < 0.1:			# reverse if close to cube
		right_wheel.setVelocity(-1.0)
		left_wheel.setVelocity(-1.0)
		return False, prev_c
	else:					# rotate when far enough from cube
		if determine_if_block(dist, bad_blocks, gps, compass):		# looking at cube
			right_wheel.setVelocity(-0.1)
			left_wheel.setVelocity(0.1)
			p2 = sf.get_object_position(dist, gps, compass)       # edge
			robot.step(timestep)
			p1 = sf.get_object_position(dist, gps, compass)       # corner

			c = sf.find_block_center_better(p1, p2)			# cube centre location from current readings
            
			return False, c
		else:			# no longer looking at cube (i.e. passed cube corner)
			return True, prev_c
			
			
def entered_dead_zone(gps, otherPosition):
    position = pf.get_robot_position(gps)
    radius = np.linalg.norm(position - otherPosition)
    if (radius < 0.5):
            return True
    else:
            return False
                
                
def avoid_other_robot(right_wheel, left_wheel, gps, compass, otherPosition):
        position = pf.get_robot_position(gps)
        radius = np.linalg.norm(position - otherPosition)
        difference = position - otherPosition
        direction = pf.get_robot_direction(compass)
        right_wheel.setVelocity(0)
        left_wheel.setVelocity(0)
        state1 = False
        if otherPosition[1] > position[1] + 0.2 and not state1:
                if otherPosition[0] < position[0] and position[0] > 0:
                	state1 = move_to_position(right_wheel, left_wheel, gps, compass, np.array([1, position[1]]))
                elif otherPosition[0] > position[0] and position[0] > 0:
                	state1 = move_to_position(right_wheel, left_wheel, gps, compass, np.array([0.3, position[1]]))
                elif otherPosition[0] < position[0] and position[0] < 0:
                	state1 = move_to_position(right_wheel, left_wheel, gps, compass, np.array([-0.3, position[1]]))
                else:
                	state1 = move_to_position(right_wheel, left_wheel, gps, compass, np.array([-1, position[1]]))
                return not state1
        else:
                return 


# moves the robot in a circle around collection zones for avoidance
def avoidCollectionZone(right_wheel, left_wheel, gps, compass, zone_colour):
	# align perpendicular to zone centre
	if pf.get_relative_orientation(gps, compass, pf.get_zone_position(zone_colour)) > 0.05:
		r_wheel_speed, l_wheel_speed = 1.57, -1.57
	elif pf.get_relative_orientation(gps, compass, pf.get_zone_position(zone_colour)) < -0.05:
		r_wheel_speed, l_wheel_speed = -1.57, 1.57
	else:			# robot allowed to move forward when properly aligned
		r_wheel_speed, l_wheel_speed = 3.14, 3.14

	right_wheel.setVelocity(r_wheel_speed)
	left_wheel.setVelocity(l_wheel_speed)
	

def moveToMid(right_wheel, left_wheel, gps, compass, block_position, state1, state2):
	if block_position[1] > 0.6:
		state1 = True
		state2 = True
	elif block_position[0] > 0.2 and not state1:
		state1 = move_to_position(right_wheel, left_wheel, gps, compass, np.array([0.6, 0.9]))
		state2 = True
	elif block_position[0] < -0.2 and not state1:
		state1 = move_to_position(right_wheel, left_wheel, gps, compass, np.array([-0.6, 0.9]))
		state2 = True
	elif abs(block_position[0]) < 0.2:	
		if not state1:
			state1 = move_to_position(right_wheel, left_wheel, gps, compass, np.array([0.6, 0.9]))
			
		elif abs(block_position[1]) < 0.2 and not state2:
			state2 = move_to_position(right_wheel, left_wheel, gps, compass, np.array([0.6, 0]))
			
		elif block_position[1] < -0.6 and not state2:
			state2 = move_to_position(right_wheel, left_wheel, gps, compass, np.array([0.6, 0.9]))
			
	return state1, state2			
			
def returnToReset(right_wheel, left_wheel, gps, compass, p, state3, state4):
	bot_position = pf.get_robot_position(gps)			
	if abs(bot_position[0]) < 0.2 and bot_position[1] < 0.6:
		state4 = move_to_position(right_wheel, left_wheel, gps, compass, np.array([0.6, bot_position[1]]))
	elif bot_position[0] > 0.2 and bot_position[1] < 0.6 and not state3:
		state3 = move_to_position(right_wheel, left_wheel, gps, compass, np.array([0.6, 0.9]))
	elif bot_position[0] < -0.2 and bot_position[1] < 0.6 and not state3:
		state3 = move_to_position(right_wheel, left_wheel, gps, compass, np.array([-0.6, 0.9]))
	elif not state4:
		state3 = True
		state4 = move_to_position(right_wheel, left_wheel, gps, compass, p)
	if state3 and state4:
		return True
	else:
		return False
		
		
def edgeCheck(right_wheel, left_wheel, gps, compass, block_position):
	state = False
	if block_position[0] > 1.1:
		state = move_to_position(right_wheel, left_wheel, gps, compass, np.array([0.9, block_position[1]]))
	elif block_position[0] < -1.1:
		state = move_to_position(right_wheel, left_wheel, gps, compass, np.array([-0.9, block_position[1]]))
	elif block_position[1] > 1.1:
		state = move_to_position(right_wheel, left_wheel, gps, compass, np.array([block_position[0], 0.9]))	
	elif block_position[1] < -1.1:
		state = move_to_position(right_wheel, left_wheel, gps, compass, np.array([block_position[0], -0.9]))
	else:
		state = True
	return state		