"""
idp_controller
controller to be used for the robots
"""

# import modules
from controller import Robot, Motor, DistanceSensor, LED, GPS, Emitter
import idp_controller_navigationmapping as navmap
import idp_controller_cubecontrol as cubecontrol
import numpy as np
import sensor_functions as sf
import position_functions as pf

# get the time step of the current world.
TIME_STEP = 8
colour = 'green'

# get the Robot instance.
robot = Robot()

# initialise devices
ds = []
dsNames = ['ds1']
for i in range(len(dsNames)):
    ds.append(robot.getDevice(dsNames[i]))
    ds[i].enable(TIME_STEP)

ls = []
lsNames = ['ls1']
for i in range(len(lsNames)):
	ls.append(robot.getDevice(lsNames[i]))
	ls[i].enable(TIME_STEP)

leds = []
ledsNames = ['led1']
for i in range(len(ledsNames)):
	leds.append(robot.getDevice(ledsNames[i]))
	leds[i].set(0)

gpss = []
gpssNames = ['gps1']
for i in range(len(gpssNames)):
	gpss.append(robot.getDevice(gpssNames[i]))
	gpss[i].enable(TIME_STEP)

compasses = []
compassesNames = ['compass1']
for i in range(len(compassesNames)):
	compasses.append(robot.getDevice(compassesNames[i]))
	compasses[i].enable(TIME_STEP)

emitters = []
emittersNames = ['emitter1']
for i in range(len(emittersNames)):
        emitters.append(robot.getDevice(emittersNames[i]))

receivers = []
receiversNames = ['receiver1']
for i in range(len(receiversNames)):
        receivers.append(robot.getDevice(receiversNames[i]))
        receivers[i].enable(TIME_STEP)

# initialise motors
wheels = []
wheelsNames = ['wheel_right', 'wheel_left']
for i in range(len(wheelsNames)):
    wheels.append(robot.getDevice(wheelsNames[i]))
    wheels[i].setPosition(float('inf'))
    wheels[i].setVelocity(0.0)

grabbers = []
grabberNames = ['grabber_right', 'grabber_left']
for i in range(len(grabberNames)):
	grabbers.append(robot.getDevice(grabberNames[i]))
	grabbers[i].setPosition(-0.3)
	grabbers[i].setVelocity(float('inf'))


# main loop:

# initialise flags
cubeGrabbed = False
cubeSpotted = False
cubeCentred = False
needsReset = False
closeToCube = True
atResetPos = False
deadZone = False
avoidRobot1 = False
avoidRobot2 = False
initialState = False
midState1 = False
midState2 = False
resetState1 = False
resetState2 = False
endState1 = False
endState2 = False
ignoreZoneFlag = False
edgeCheck = False
needGrab = 2

# initialise variables
cubeSearchCounter = 0
cubeCentredCounter = 0
blockCounter = 0
initialPauseCounter = 20
badCubes = []
#prev_c = np.array([0, 0])
robotDirection = np.array([0, 0])
otherPosition = np.array([0, 0])
otherDirection = np.array([0, 0])
backup = np.array([0,0])

# wait 1 timestep to initialise more start variables
robot.step(TIME_STEP)
startPosition = np.array([gpss[0].getValues()[0], gpss[0].getValues()[2]])
zonePosition = pf.get_zone_position(colour)
resetPosition = np.array([0, -0.9])


while initialPauseCounter > 0:
        robot.step(TIME_STEP)
        initialPauseCounter -= 1

print("GREEN: START!")

# main loop
while robot.step(TIME_STEP) != -1:

	# get sensor readings
	pulsewidth = ds[0].getValue()
	dist = sf.ultrasonicDistance(pulsewidth)
	p = sf.get_object_position(dist, gpss[0], compasses[0])
	light = ls[0].getValue()
	sf.send_coordinate_data(gpss[0], compasses[0], emitters[0])
	otherPosition, otherDirection = sf.recieve_coordinate_data(otherPosition, otherDirection, receivers[0])
	
	if not initialState:
		initialState = navmap.move_to_position(wheels[0], wheels[1], gpss[0], compasses[0], resetPosition)
	
	elif blockCounter < 4:
	
		nearZoneFlag, nearZoneColour = pf.nearCollectionZone(gpss[0], compasses[0])

		if navmap.entered_dead_zone(gpss[0], otherPosition) and not deadZone:
			deadZone = True
			
		elif deadZone and not avoidRobot1:
			avoidRobot1 = navmap.avoid_other_robot(wheels[0], wheels[1], gpss[0], compasses[0], otherPosition, otherDirection, avoidRobot1)
			if avoidRobot1:
				if not cubeGrabbed:
					needsReset = True
				
	
		elif not needsReset:			# complete main loop if no reset is needed
			cubecontrol.close_grabbers(grabbers[0], grabbers[1])
			if not cubeSpotted:			# rotate until cube spotted
				cubeSpotted = navmap.searchForCube(wheels[0], wheels[1], gpss[0], compasses[0], dist, badCubes, otherPosition)

				cubeSearchCounter += 1				# counts time of robot search
				if cubeSearchCounter > 1200:		# if the robot has been searching for 2 full rotations then move to new position to search
					resetPosition = np.array([-0.9, 0])
					initialState = False					
			
			if cubeSpotted and not cubeGrabbed:			# cube spotted but not collected by robot
				if not cubeCentred:						# rotate to centre on cube
					cubeCentredCounter, cubeCentred, cubePosition, backup = navmap.centreOnCube(wheels[0], wheels[1], gpss[0], compasses[0], dist, cubeCentredCounter, TIME_STEP, badCubes, otherPosition, backup)
					robotResetDirection = pf.get_robot_direction(compasses[0])
					
				elif not midState1 or not midState2:
					midState1, midState2 = navmap.moveToMid(wheels[0], wheels[1], gpss[0], compasses[0], cubePosition, midState1, midState2)			
	
				elif not edgeCheck:
					edgeCheck = navmap.edgeCheck(wheels[0], wheels[1], gpss[0], compasses[0], cubePosition)
				
				else:									# centred on cube so move to cube
	
					if np.linalg.norm(pf.get_robot_position(gpss[0]) - cubePosition) < 0.2:
						ignoreZoneFlag = True
						cubecontrol.open_grabbers(grabbers[0], grabbers[1])				# ignore any collection zones when close to a cube
					elif ignoreZoneFlag and np.linalg.norm(pf.get_robot_position(gpss[0]) - cubePosition) < 0.25:
						ignoreZoneFlag = True				# if already told to ignore collection zone and still close-ish to cube, keep ignoring zones
						cubecontrol.open_grabbers(grabbers[0], grabbers[1])
					elif np.linalg.norm(pf.get_robot_position(gpss[0]) - cubePosition) < 0.4:
						cubecontrol.open_grabbers(grabbers[0], grabbers[1])
					else:
						ignoreZoneFlag = False				# far away from cube so pay attention to zones
	
					needGrab = navmap.checkCube(wheels[0], wheels[1], gpss[0], compasses[0], dist, light, colour, cubePosition, ignoreZoneFlag)			# check cube colour
					cubeSearchCounter += 1

					if needGrab == 0:		# cube incorrect colour
						badCubes.append(sf.find_block_center(dist, gpss[0], compasses[0]))
						needsReset = True
					elif needGrab == 1:		# cube correct colour
						cubecontrol.close_grabbers(grabbers[0], grabbers[1])
						needGrab = 2
						cubeGrabbed = True
					elif needGrab == 3:
						needsReset = True
					else:
						pass
	
			elif cubeGrabbed:		# return to base when cube grabbed
	
				# avoid opposite collection zone only when returing to collection zone
				if nearZoneColour != colour and nearZoneFlag and np.linalg.norm(pf.get_robot_position(gpss[0]) - pf.get_zone_position(nearZoneColour)) < 0.3:
					navmap.avoidCollectionZone(wheels[0], wheels[1], gpss[0], compasses[0], nearZoneColour)
					atBase = False
				else:
					atBase = navmap.move_to_position(wheels[0], wheels[1], gpss[0], compasses[0], zonePosition)
	
				if atBase:			# release cube when back at base
					wheels[0].setVelocity(0)
					wheels[1].setVelocity(0)
					cubecontrol.open_grabbers(grabbers[0], grabbers[1])
					
					blockCounter += 1			# iterate block counter, decide if any more blocks need to be collected
	
					needsReset = True
		
		else:			# need to reset robot for next search
			if dist < 0.3 and closeToCube:		# close to a cube so need to reverse
				wheels[0].setVelocity(-3.14)
				wheels[1].setVelocity(-3.14)
			else:
				closeToCube = False
				cubecontrol.close_grabbers(grabbers[0], grabbers[1])
				if not atResetPos:		# moves back to start position
					# avoids collection zones it may encounter
					if nearZoneFlag and np.linalg.norm(pf.get_robot_position(gpss[0]) - pf.get_zone_position(nearZoneColour)) < 0.3:
						navmap.avoidCollectionZone(wheels[0], wheels[1], gpss[0], compasses[0], nearZoneColour)
					else:
						atResetPos = navmap.returnToReset(wheels[0], wheels[1], gpss[0], compasses[0], resetPosition, resetState1, resetState2)
				else:
					needsReset = not(navmap.point_towards(wheels[0], wheels[1], compasses[0], robotResetDirection))
					if not needsReset:
						cubeGrabbed = False		# resets flags and variables for next run
						cubeSpotted = False
						cubeCentred = False
						closeToCube = True
						atResetPos = False
						ignoreZoneFlag = False
						midState1 = False
						midState2 = False
						resetState1 = False
						resetState2 = False
						atCube = False
						needGrab = 2
						robot_direction = 0
						cubeCentredCounter = 0
						cubeSearchCounter = 0
						deadZone = False
						avoidRobot1 = False
						edgeCheck = False
						
						
	else:
            	if dist < 0.3 and closeToCube and not endState1:		# close to a cube so need to reverse
            			wheels[0].setVelocity(-3.14)
            			wheels[1].setVelocity(-3.14)
            	
            	elif not endState2:
            		endState2 = navmap.returnToReset(wheels[0], wheels[1], gpss[0], compasses[0], resetPosition, resetState1, resetState2)
            		endState1 = True
            	else:            				
            		if navmap.move_to_position(wheels[0], wheels[1], gpss[0], compasses[0], startPosition):
            			wheels[0].setVelocity(0)
            			wheels[1].setVelocity(0)
            			break
	
	
	

# end of program
print('{} robot has finished!'.format(colour))