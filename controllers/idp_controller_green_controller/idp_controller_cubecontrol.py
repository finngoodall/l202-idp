"""
Contains functions needed for cube control (grabbing, releasing, etc) for the robot
"""


def open_grabbers(right_grabber, left_grabber):
	right_grabber.setPosition(-0.3)
	left_grabber.setPosition(-0.3)


def close_grabbers(right_grabber, left_grabber):
	right_grabber.setPosition(0.05)
	left_grabber.setPosition(0.05)



