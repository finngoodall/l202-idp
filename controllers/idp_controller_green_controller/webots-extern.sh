# Used if you want to run Webots controllers from a command line (was used to debug issues with different linux installs)
# To use this set the robot's controller to <extern> in Webots and start playing the simulation (the robot shouldn't do anything)
# Now do '. ./webots-extern.sh' in your terminal to use the external controller (the robot should do what the controller says)

export WEBOTS_HOME=/usr/local/webots
export LD_LIBRARY_PATH=$WEBOTS_HOME/lib/controller
export PYTHONPATH=$WEBOTS_HOME/lib/controller/python38
export PYTHONENCODING=utf-8

